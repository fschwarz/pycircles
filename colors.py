#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 13:49:28 2019

@author: fschwarz
"""

import random

WHITE = (255, 255, 255)
BLACK, RED = (0, 0, 0), (255, 128, 128)
GREEN = (128, 255, 128)
GREENTREE = (96, 192, 96, 192)
BROWN = (128, 64, 64)
BLUE = (192, 192, 255)
YELLOW = (255, 255, 0)


def getRandomGreenTree():
        return (GREENTREE[0]+ random.randint(0, 128), GREENTREE[1], GREENTREE[2] + random.randint(0, 32), GREENTREE[3])