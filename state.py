#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 19:09:43 2019

@author: fschwarz
"""

class State:
    def __init__(self):
        self.nextState = self
        
    def setNextState(self, state):
        self.nextState = state
        