#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 18:59:25 2019

@author: fschwarz
"""

from titlescreen import TitleScreen
from scene import Scene


class Game:
    def __init__(self):
        self.state = TitleScreen()
    
    def keysHandler(self, keys):
        self.state.keysHandler(keys)
        
    def draw(self, screen):
        self.state = self.state.nextState
        self.state.draw(screen)