#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 13:49:45 2019

@author: fschwarz
"""

from pyrr import Vector3


class Ball:
    def __init__(self, c: Vector3, r, color):        
        self.c = c;
        self.r = r;
        self.color = color;
   