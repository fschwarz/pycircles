#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 14:18:34 2019

@author: fschwarz
"""

from sceneobject import SceneObject
from colors import *
from ball import Ball
from pyrr import Vector3
import matrix
from physicalpoint import PhysicalPoint

class Hero(SceneObject):
    def __init__(self, pos):
        super().__init__()
        self.circlife = 0
        self.physicalPoint = PhysicalPoint(pos)
        self.angle = 0
        self.circlifes = []
        self.body = Ball(Vector3([pos.x, pos.y, pos.z]), 64, YELLOW)
        self.noose = Ball(Vector3([pos.x, pos.y, pos.z]), 16, YELLOW)
        self.live()
        self.addBall(self.body)
        self.addBall(self.noose)
        
    def getPos(self):
        return self.physicalPoint.getPosition()
    
    def live(self):
        self.physicalPoint.live()
        self.body.c = self.getPos()
        self.noose.c = self.getPos() + matrix.vectorDir(64, 0, 0, self.angle)
        
        angle = 1
        for circlife in self.circlifes:
            circlife.c = self.getPos() + matrix.vectorDir(64-16, 0, 0, self.angle + angle)
            angle += 1
        
    def move(self):
        self.physicalPoint.setA(matrix.vectorDir(32, 0, 0, self.angle))
        #self.pos = self.pos + matrix.vectorDir(16, 0, 0, self.angle)
        self.live()
        
    def back(self):
        self.physicalPoint.setA(matrix.vectorDir(-4, 0, 0, self.angle))
        #self.pos = self.pos + matrix.vectorDir(-4, 0, 0, self.angle)
        self.live()
        
    def jump(self):
        if self.physicalPoint.pos.z == 0:
            self.physicalPoint.setA(Vector3([0, 0, 100]))
        
    def turnLeft(self):
        self.angle -= 0.2
        self.live()
        
    def turnRight(self):
        self.angle += 0.2
        self.live()
        
    def addCirclife(self):
        ball = Ball( Vector3([self.getPos().x, self.getPos().y, self.getPos().z]) + matrix.vectorDir(64-48, 0, 0, self.circlife), 16, BLUE)
        self.circlifes.append(ball)
        self.addBall(ball)