#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Tue Aug  6 17:04:53 2019

@author: fschwarz
"""


from sceneobject import SceneObject
from ball import Ball
from pyrr import Vector3
import random
import math

CIRCLEDOORBOTTOM = (192, 0, 0, 255)
CIRCLEDOOR = (164, 164, 192, 192)

class CircleDoor(SceneObject):
    def __init__(self, pos):
        super().__init__()
        RADIUS = 128
        self.pos = pos
        self.addBall(Ball(Vector3([pos.x, pos.y, pos.z+RADIUS/2]), RADIUS, CIRCLEDOOR))
        BOTTONBALLRADIUS = 12
        BOTTOMRADIUS =  RADIUS * 6 / 7
        for i in range(0, 40):
            angle = i * 2 * math.pi / 40
            self.addBall(Ball(Vector3([pos.x + BOTTOMRADIUS*math.cos(angle), pos.y + BOTTOMRADIUS*math.sin(angle), pos.z+BOTTONBALLRADIUS/2]), BOTTONBALLRADIUS, CIRCLEDOORBOTTOM))
        