#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 20:35:16 2019

@author: fschwarz
"""

from pyrr import Vector3

dt = 0.5

GRAVITY = 12

class PhysicalPoint:
    def __init__(self, pos):
        self.pos = pos;
        self.v = Vector3([0.0, 0.0, 0.0])
        self.a = Vector3([0.0, 0.0, 0.0])
        
    def live(self):
       
        self.v += dt * self.a
        self.pos += dt * self.v
        self.pos.z = max(self.pos.z, 0)
        self.a = 0.5 * self.a
        if(self.a.z > -GRAVITY):
            self.a.z -= GRAVITY
        self.v = 0.7 * self.v
        
        
    def setA(self, a):
        self.v = 0
        self.a = a
        
    def setPosition(self, pos):
        self.pos = pos
        
    def getPosition(self):
        return self.pos