#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 31 20:59:39 2019

@author: fschwarz
"""


from pyrr import Vector3, Matrix44, vector, vector3
from camera import Camera
import pygame
import math
import numpy
from objects.tree import Tree
from ball import *
from colors import *
from objects.hero import Hero
import random
from house import House
from objects.circlife import Circlife
from objects.circledoor import CircleDoor
from state import State
import music


ECART = 32

class Scene(State):
        camera = Camera()
        objects = []
        hero = Hero(Vector3([-64.01, 0.01, 0]))
        
        
        def __init__(self):
            super().__init__()
            music.play("Circles_-_main_theme.ogg")
            
            for i in range(0, 20):
                self.addObject(Tree(Vector3([random.randint(0, 1500), random.randint(0, 1000), 0]), random.randint(128, 200), random.randint(48, 70)))
           
            for i in range(0, 6):
                self.addObject(Circlife(Vector3([random.randint(0, 2000), random.randint(0, 1000), 0])))
            
            self.addObject(House(Vector3([-600, 0, 0]), 100))
            self.addObject(CircleDoor(Vector3([-200, 200, 0])))
           
        def getBalls(self):  
            balls = []                
            for obj in self.objects:
                    for ball in obj.getBalls():
                        balls.append(ball)
            for ball in self.hero.getBalls():
                balls.append(ball)            
            return balls
                        
        def addObject(self, obj):
            self.objects.append(obj)
            
        def takeZIntoAccount(self, x, z):
            factor = 500
            return factor * x / (factor - z)
        
        def getPointInScreen(self, matrix, pos):
            pos2 = matrix * pos;
            return Vector3([self.takeZIntoAccount(pos2.x, pos2.z), self.takeZIntoAccount(pos2.y, pos2.z), pos2.z]);
        
        
        def drawCircle(self, screen, color, x, y, r):
            if x - r > 800:
                return
            
            if y - r > 600:
                return
            
            if y + r < 0:
                return
            
            if x + r < 0:
                return 
            
            if r > 300:
                return
            if r < 3:
                return
            surface = pygame.Surface((2*r, 2*r), pygame.SRCALPHA, 32)
            pygame.draw.circle(surface, color, (r, r), r)
            screen.blit(surface, (x-r, y-r))
            del surface
           # pygame.draw.circle(screen, color, (x, y), r)
            if r > 3:
                pygame.draw.circle(screen, BLACK, (x, y), r, 3)
        
        def drawBall(self, screen, matrix, ball):
            c = self.getPointInScreen(matrix, ball.c);
            
            if numpy.isnan(c.x): return
            if numpy.isnan(c.y): return
            if numpy.isnan(c.z): return
            
            self.drawCircle(screen, ball.color, 200+int(c.x), 300-int(c.y), int(self.takeZIntoAccount(ball.r, c.z)))
            
        def draw(self, screen):
                screen.fill(GREEN)
                
                for obj in self.objects:
                    obj.live()
                    
                self.hero.live()
                
                self.collisionTests()
                
                balls = self.getBalls()
            
                self.camera.positionate(self.hero.getPos());
                matrix = self.camera.getMatrix()
                        
                for ball in balls:
                    ball.screenpos = self.getPointInScreen(matrix, ball.c)
                    
                balls.sort(key = lambda ball : ball.screenpos.z)
                
                for ball in balls:
                    self.drawBall(screen, matrix, ball)
                    
                    
        def cameraLeft(self):
            self.camera.turnLeft()

        def cameraRight(self):
            self.camera.turnRight()
            
        def goLeft(self):
            self.hero.turnLeft()
            
        def goRight(self):
            self.hero.turnRight()
            
        def goUp(self):
            self.collisionTests()
            self.hero.move()
            
        def jump(self):
            self.collisionTests()
            self.hero.jump()
            
        def goDown(self):
            self.collisionTests()
            self.hero.back()
            
            
        def collisionTests(self):
            for obj in self.objects:
              for ball in obj.balls:
                    if(isBallCollide(self.hero.body, ball)):
                        obj.collisionEffect(ball, self.hero)
                        if(isinstance(obj, Circlife)):
                            if(obj in self.objects):
                               self.objects.remove(obj)
                    
            
        def keysHandler(self, keys):
            if keys[pygame.K_ESCAPE]:
                pygame.quit()
            if keys[pygame.K_LEFT]:
                self.goLeft()
            if keys[pygame.K_RIGHT]:
                self.goRight()
            if keys[pygame.K_UP]:
                self.goUp()
            if keys[pygame.K_DOWN]:
                self.goDown()
            if keys[pygame.K_w]:
                self.cameraLeft()
            if keys[pygame.K_c]:
                self.cameraRight()
            if keys[pygame.K_SPACE]:
                self.jump()
                
                    
def norm(v):
    return math.sqrt(v.x * v.x + v.y * v.y + v.z * v.z)


def isBallCollide(b1, b2):
    v = b1.c - b2.c
    d = norm(v)
    return d < b1.r + b2.r
           
        
