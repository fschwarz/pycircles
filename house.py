#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 16:39:32 2019

@author: fschwarz
"""



from sceneobject import SceneObject
from ball import Ball
from pyrr import Vector3
from colors import *
import matrix
import random
import math

class House(SceneObject):
        def __init__(self, pos, h):
            super().__init__()
            self.h = h
            self.pos = pos;
            self.house = Ball(Vector3([pos.x, pos.y, pos.z + h/2]), h, WHITE) 
            self.addBall(self.house)
            
        
            