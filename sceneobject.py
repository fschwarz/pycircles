#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Thu Aug  1 13:37:29 2019

@author: fschwarz
"""

class SceneObject:
    def __init__(self):
        self.balls = []
    
    def addBall(self, ball):
                self.balls.append(ball);
                
    def getBalls(self):
        return self.balls;
    
    def live(self):
            pass
        
    def collisionEffect(self, ball, hero):
        v = (hero.body.c - ball.c)
        v.z = 0
        v.normalize()
        hero.physicalPoint.setA(v * 40)
        hero.physicalPoint.setPosition(ball.c + v * (ball.r + hero.body.r ))
