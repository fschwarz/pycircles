#!/usr/bin/env python3
# -*- coding: utf-8 -*-
"""
Created on Wed Jul 31 21:04:59 2019

@author: fschwarz
"""

from pyrr import Vector3
import matrix


class Camera:
    def __init__(self):
        self.target = Vector3([0, 0, 0])
        self.angle = 0.0

    def getMatrix(self):
        cameraPosition = (self.target + matrix.rotationMatrix(0, 0, self.angle) * Vector3([3, -32, 32]))
        return matrix.lookAtMatrix(cameraPosition, self.target, Vector3([0, 0, 1]))

    def positionate(self, target):
        self.target = target
            
    def turnLeft(self):
        self.angle = self.angle - 0.1
        
    def turnRight(self):
        self.angle += 0.1
